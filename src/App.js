import "./index.css";
import "font-awesome/css/font-awesome.min.css";
import avatar from "./avatar.JPG";

function App() {
  return (
    <div className="container">
      <div className="info">
        <img src={avatar} alt="" className="img" />
        <div className="full-name" style={{ color: "#C1E3E1" }}>
          PHẠM TRUNG HIẾU
        </div>
        <div className="personalInfo">
          <i className="fa fa-birthday-cake icon" style={{ color: "white" }} />
          <p style={{ color: "#C1E3E1" }}>20-10-1999</p>
          <i className="fa fa-male icon" style={{ color: "white" }} />
          <p style={{ color: "#C1E3E1" }}>Nam</p>
          <i className="fa fa-envelope icon" style={{ color: "white" }} />
          <p style={{ color: "#C1E3E1" }}>phamtrunghieuxtba8@gmail.com</p>
          <i class="fa fa-phone icon" style={{ color: "white" }} />
          <p style={{ color: "#C1E3E1" }}>0346932126</p>
          <i className="fa fa-map-marker icon" style={{ color: "white" }} />
          <p style={{ color: "#C1E3E1" }}>Kim Ngưu-Hai Bà Trưng-Hà Nội</p>
        </div>
      </div>
      <div className="info-cv">
        <h2>Mục tiêu nghề nghiệp</h2>
        <p>Trở thành fullstack developer sau 2 năm</p>
        <h2 style={{ marginBottom: "0" }}>Học vấn</h2>
        <div className="education">
          <h4>HỌC VIỆN CÔNG NGHỆ BƯU CHÍNH VIỄN THÔNG</h4>
          <p className="text">Chuyên ngành: Công nghệ thông tin</p>
        </div>
        <p style={{ marginTop: "0" }}>Tốt nghiệp vào tháng 12 năm 2021</p>
        <h2>Kỹ năng</h2>
        <div className="skill">
          <h4>Ngôn ngữ</h4>
          <p>Java, C, C++, JavaScript</p>
          <h4>Backend</h4>
          <p>Java Spring boot with restfulAPI, graphql, thymeleaf</p>
          <h4>Frontend</h4>
          <p>HTML, CSS, Bootstrap, JQuery, ReactJs</p>
          <h4>Database</h4>
          <p>MySQL, SQLServer</p>
          <h4>Version Control</h4>
          <p>Git</p>
          <h4>Khác</h4>
          <p>Có kiến thức oop. Có khả năng tham gia thiết kế hệ thống.</p>
          <h4>Tiếng anh</h4>
          <p>TOEIC 890</p>
        </div>
        <h2>Kinh nghiệm</h2>
        <div className="skill">
          <h4>15/12/2020 - 30/02/2021</h4>
          <p>Thực tập Java Spring Boot</p>
        </div>
      </div>
    </div>
  );
}

export default App;
